// Core
import React from 'react';
import ReactDOM from 'react-dom';

// Theme
import './theme/init';
import {Instagram} from './pages/instagram'



ReactDOM.render(<Instagram/>, document.getElementById('app'));
