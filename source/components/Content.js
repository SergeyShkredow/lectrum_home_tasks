import React from "react";
import {Profile} from "./Profile";
import {Images} from "./Images";
import {Stories} from "./Stories";

export class Content extends React.Component {
    render () {
        return (
            <>
                <div className='content'>
                    <Profile/>
                    <Stories/>
                    <Images/>
                </div>
           </>
        )
    }
}