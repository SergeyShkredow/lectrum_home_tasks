import React from "react";
import links from '../theme/assets/links'

export class Footer extends React.Component {
    render () {
        const linksJsx = links.map((link) =>
            <li key = {link.id}>
                 {link.message}
            </li>);
        return (
            <>
                <div className='footer'>
                   <ul>
                       {linksJsx}
                   </ul>
                </div>
            </>
        )
    }
}